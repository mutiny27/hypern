import os
import platform
import random
import threading
import socket
import pyperclip
import argparse
import logging
import sys
import subprocess
import time
import ipwhois
import itertools
import HypernCore as hc

hc.clear()
print("Hypern Version 1.2.1")
print("Created by Mutiny27")
print("Follow on Twitter: _Mutiny27_")

run = input("Would you like to run Hypern? Y/N: ")

if run == 'y':
    askLoop = input("Would you like the program to close after running a test? Y/N: ")
    if askLoop == 'y':
        loop = 1

    if askLoop == 'n':
        loop = 100000000

    while loop > 0:
        loop = loop - 1
        hc.clear()

        print("Hypern")
        print("1. Internet tools")
        print("2. Information Gathering")
        print("3. Exploitation")
        print("4. Passwords")
        print("5. Calculator")
        print("0. Exit")

        choice1 = input("Enter the tool which you want to use 1/2/3/4/5: ")
        if choice1 == '1':
            hc.clear()
            print("Select a tool")
            print("1. Port Scanner")
            print("2. IP Finder")
            print("3. traceroute")
            print("0. Back")

            choice2 = input("Enter choice 1/2/3/4/0: ")
            if choice2 == '1':
                hc.clear()
                hc.PortScanner()

            if choice2 == '2':
                hc.clear()
                hc.IPFinder()

            if choice2 == '3':
                hc.clear()
                hc.traceRoute()

        # INFORMATION GATHERING CHOICE MENU

        if choice1 == '2':
            hc.clear()
            print("Information Gathering")
            print("1. NMAP")
            print("0. Back")
            INFO = input("Enter the tool you want 1/0: ")

            # NMAP MENU

            if INFO == '1':
                hc.clear()
                print("NMAP Options")
                print("1. Quick Scan")
                print("2. Regular Scan")
                print("3. Intense Scan")
                print("4. Comprehensive Scan")
                print("0. Back")
                hc.nmap()

        # EXPLOITATION MENU

        if choice1 == '3':
            hc.clear()
            print("Exploitation")
            print("1. Sqlmap")
            print("2. Metasploit")
            exploitChoice = input("Enter the option you wish to use 1: ")

            # SQLMAP OPTIONS

            if exploitChoice == '1':
                hc.clear()
                print("Sqlmap")
                print("1. sqlmap wizard")
                print("2. sqlmap shell")
                print("3. Manual")
                print("0. Back")
                hc.sqlMap()

            # METASPLOIT MENU

            if exploitChoice == '2':
                print("Metasploit")
                print("1. Wizard")
                print("2. Manual")
                print("0. Back")
                hc.metasploit()

        # PASSWORD MENU BEGINNING

        if choice1 == '4':
            clear()
            print("1. Password Cracker")
            print("0. Back")

            passwordChoice = input("Enter one of the options 1/0: ")

            if passwordChoice == '2':
                clear()
                print("Password Cracking")
                print("1. SSH Cracker")
                print("2. FTP Cracker")
                print("3. VNC Cracker")
                print("4. TELNET Cracker")
                print("5. POP3 Cracker")
                print("0. Back")
                passwordCracker = input("Enter one of the Options 1/2/3/4/5/0: ")

                # SSH CRACKER
                if passwordCracker == '1':
                    hc.clear()
                    hc.sshCracker()

                # FTP Cracker
                if passwordCracker == '2':
                    hc.clear()
                    hc.ftpCracker()

                # VNC Cracker
                if passwordCracker == '3':
                    hc.clear()
                    hc.vncCracker()

                # TELNET Cracker
                if passwordCracker == '4':
                    hc.clear()
                    hc.telnetCracker()

                # POP3 Cracker
                if passwordCracker == '5':
                    hc.clear()
                    hc.pop3Cracker()

        if choice1 == '5':
            hc.clear()
            print("Calculator")
            print("1. Addition")
            print("2. Subtraction")
            print("3. Multiplication")
            print("4. Division")
            print("5. Averages")
            print("0. Back")
            mathChoice = input("Enter one of the options 1/2/3/4/5/0: ")

            if mathChoice == '1':
                hc.clear()
                hc.addition()

            if mathChoice == '2':
                hc.clear()
                hc.subtraction()

            if mathChoice == '3':
                hc.clear()
                hc.multiplication()

            if mathChoice == '4':
                hc.clear()
                hc.division()

            if mathChoice == '5':
                hc.clear()
                hc.average()

        if choice1 == '0':
            clear()
            print("Goodbye...")
            time.sleep(1)
            quit()



elif run == 'n':
    clear()
    print("Goodbye...")
    time.sleep(1)
    quit()